import { rendererStore } from '../../store';
import { Choice } from '../../../main/services/testycool/choice';
import {
  listChoices,
  setError,
} from '../../../shared/redux/actions/ChoiceAction';
import { NumberToFormatModel } from '../../../shared/libs/TextFormatTypeParser';

export default function choiceListener() {
  window.choice.ipcRenderer.on(
    'list-choices',
    (response: {
      size: number;
      page: number;
      choices: Choice[];
      totalSize: number;
    }) => {
      rendererStore.dispatch(
        listChoices.fulfilled({
          ...response,
          choices: response.choices.map((choice) => ({
            ...choice,
            format: NumberToFormatModel(choice.format),
          })),
        })
      );
    }
  );
  window.choice.ipcRenderer.on(
    'choice-error',
    (response: { code: number; details: string }) => {
      console.log('error');
      rendererStore.dispatch(setError({ err: response }));
    }
  );
}
