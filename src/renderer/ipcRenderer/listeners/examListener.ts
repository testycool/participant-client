import { StatusNumberToStatusModel } from '../../../shared/libs/StatusParser';
import { rendererStore } from '../../store';
import { getExam, setError } from '../../../shared/redux/actions/ExamAction';
import { GetExamResponse } from '../../../main/services/testycool/exam';

export default function examListener() {
  window.exam.ipcRenderer.on('get-exam', (args: GetExamResponse) => {
    if (args.exam) {
      rendererStore.dispatch(
        getExam.fulfilled({
          exam: {
            ...args.exam,
            startAt: args.exam.startAt?.toISOString(),
            status: StatusNumberToStatusModel(args.exam.status),
          },
        })
      );
    }
  });

  window.exam.ipcRenderer.on(
    'exam-error',
    (args: { code: number; details: string }) => {
      rendererStore.dispatch(setError({ err: args }));
    }
  );
}
