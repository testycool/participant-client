import { rendererStore } from '../../store';
import {
  getQuestion,
  listQuestions,
  setQuestionError,
} from '../../../shared/redux/actions/QuestionAction';
import { NumberToTypeModel } from '../../../shared/libs/QuestionTypeParser';
import {
  GetQuestionResponse,
  Question,
} from '../../../main/services/testycool/question';
import { NumberToFormatModel } from '../../../shared/libs/TextFormatTypeParser';
import { QuestionTypeModel } from '../../../shared/models/QuestionModel';
import { clearChoices } from '../../../shared/redux/actions/ChoiceAction';
import { requestListChoices } from '../senders/choiceSender';

export default function questionListener() {
  window.question.ipcRenderer.on(
    'list-questions',
    (response: {
      size: number;
      page: number;
      questions: Question[];
      totalSize: number;
    }) => {
      rendererStore.dispatch(
        listQuestions.fulfilled({
          ...response,
          questions: response.questions.map((question) => ({
            ...question,
            type: NumberToTypeModel(question.type),
            format: NumberToFormatModel(question.format),
          })),
        })
      );
    }
  );
  window.question.ipcRenderer.on(
    'get-question',
    (response: GetQuestionResponse) => {
      if (response.question) {
        rendererStore.dispatch(
          getQuestion.fulfilled({
            question: {
              ...response.question,
              type: NumberToTypeModel(response.question.type),
              format: NumberToFormatModel(response.question.format),
            },
          })
        );
        if (
          NumberToTypeModel(response.question.type) === QuestionTypeModel.MC
        ) {
          requestListChoices(-1, 1, response.question.id);
        } else {
          rendererStore.dispatch(clearChoices());
        }
      }
    }
  );
  window.question.ipcRenderer.on(
    'question-error',
    (args: { code: number; details: string }) => {
      rendererStore.dispatch(setQuestionError({ err: args }));
    }
  );
}
