import { rendererStore } from '../../store';
import { setError } from '../../../shared/redux/actions/AuthAction';
import {
  GetParticipantResponse,
  Participant,
} from '../../../main/services/testycool/participant';
import { getParticipant } from '../../../shared/redux/actions/ParticipantAction';

export default function participantListener() {
  window.participant.ipcRenderer.on(
    'list-participants',
    (response: {
      size: number;
      page: number;
      participants: Participant[];
      totalSize: number;
    }) => {
      console.log(response);
    }
  );

  window.participant.ipcRenderer.on(
    'get-participant',
    (response: GetParticipantResponse) => {
      if (response.participant) {
        rendererStore.dispatch(
          getParticipant.fulfilled({
            participant: {
              ...response.participant,
            },
          })
        );
      }
    }
  );

  window.participant.ipcRenderer.on(
    'participant-error',
    (args: { code: number; detail: string }) => {
      rendererStore.dispatch(setError({ err: args }));
    }
  );
}
