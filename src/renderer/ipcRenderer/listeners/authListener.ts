import { rendererStore } from '../../store';
import {
  setAuthStatus,
  setError,
} from '../../../shared/redux/actions/AuthAction';
import { requestGetExam } from '../senders/examSender';
import { requestGetParticipant } from '../senders/participantSender';

export default function authListener() {
  window.auth.ipcRenderer.on(
    'get-token',
    (args: {
      accessToken: string;
      examPassword: string;
      participantId: string;
    }) => {
      if (args.accessToken) {
        rendererStore.dispatch(setAuthStatus({ authenticated: true }));
        requestGetExam(args.examPassword);
        requestGetParticipant(args.participantId);
      }
    }
  );
  window.auth.ipcRenderer.on(
    'auth-error',
    (args: { code: number; detail: string }) => {
      rendererStore.dispatch(setError({ err: args }));
    }
  );
}
