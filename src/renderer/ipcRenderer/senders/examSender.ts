import { rendererStore } from '../../store';
import { getExam } from '../../../shared/redux/actions/ExamAction';

// eslint-disable-next-line import/prefer-default-export
export const requestGetExam = (examPassword: string) => {
  rendererStore.dispatch(getExam.pending());
  window.exam.ipcRenderer.getExam(examPassword);
};
