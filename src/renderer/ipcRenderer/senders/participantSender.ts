// eslint-disable-next-line import/prefer-default-export
import { rendererStore } from '../../store';
import { getParticipant } from '../../../shared/redux/actions/ParticipantAction';

export function requestListParticipants(
  size: number,
  page: number,
  examId: number
) {
  window.participant.ipcRenderer.listParticipants(size, page, examId);
}

export const requestGetParticipant = (participantId: string) => {
  rendererStore.dispatch(getParticipant.pending());
  window.participant.ipcRenderer.getParticipant(participantId);
};
