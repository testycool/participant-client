import { rendererStore } from '../../store';
import { listChoices } from '../../../shared/redux/actions/ChoiceAction';

// eslint-disable-next-line import/prefer-default-export
export const requestListChoices = (
  size: number,
  page: number,
  questionId: number
) => {
  rendererStore.dispatch(listChoices.pending());
  window.choice.ipcRenderer.listChoices(size, page, questionId);
};
