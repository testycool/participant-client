import { rendererStore } from '../../store';
import {
  getQuestion,
  listQuestions,
} from '../../../shared/redux/actions/QuestionAction';

export const requestListQuestions = (
  size: number,
  page: number,
  examId: number
) => {
  rendererStore.dispatch(listQuestions.pending());
  window.question.ipcRenderer.listQuestions(size, page, examId);
};

export const requestGetQuestion = (id: number) => {
  rendererStore.dispatch(getQuestion.pending());
  window.question.ipcRenderer.getQuestion(id);
};
