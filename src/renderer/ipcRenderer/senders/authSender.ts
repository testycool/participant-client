export const refreshAddress = (address: string) => {
  window.auth.ipcRenderer.refreshAddress(address);
};

export const getToken = (examPassword: string, participantCode: string) => {
  window.auth.ipcRenderer.getToken(examPassword, participantCode);
};
