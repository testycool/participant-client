import React from 'react';
import { MemoryRouter as Router, Switch, Route } from 'react-router-dom';
import './App.global.css';
import Login from './views/Login';
import {
  authListener,
  choiceListener,
  examListener,
  participantListener,
  questionListener,
} from './ipcRenderer/listeners';
import Waiting from './views/Waiting';
import Attempt from './views/Attempt';

declare global {
  interface Window {
    auth?: any;
    exam?: any;
    participant?: any;
    question?: any;
    choice?: any;
  }
}

authListener();
examListener();
participantListener();
questionListener();
choiceListener();

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/attempt" component={Attempt} />
        <Route path="/waiting" component={Waiting} />
        <Route path="/" component={Login} />
      </Switch>
    </Router>
  );
}
