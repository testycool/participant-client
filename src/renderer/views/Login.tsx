import React, { useEffect, useState } from 'react';
import {
  Button,
  Card,
  Form,
  Input,
  Layout,
  notification,
  Row,
  Space,
  Typography,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { getToken, refreshAddress } from '../ipcRenderer/senders/authSender';
import { AppDispatch, RootState } from '../store';
import { setError } from '../../shared/redux/actions/AuthAction';

export default function Login(): JSX.Element {
  const { Content } = Layout;
  const { Title } = Typography;

  const authStatus = useSelector(
    (state: RootState) => state.AuthReducer.authStatus
  );
  const authError = useSelector((state: RootState) => state.AuthReducer.error);

  const loginRequest = (values: any) => {
    getToken(values.examPassword, values.participantCode);
  };

  const [loginForm, setLoginForm] = useState({
    address: {
      text: '',
      disabled: false,
    },
    examPassword: {
      text: '',
      disabled: true,
    },
    participantCode: {
      text: '',
      disabled: true,
    },
  });

  const updateForm = (_changedValues: any, allValues: any) => {
    setLoginForm((prevState) => {
      return {
        ...prevState,
        address: {
          ...prevState.address,
          text: allValues.address,
        },
        examPassword: {
          ...prevState.examPassword,
          text: allValues.examPassword,
        },
        participantCode: {
          ...prevState.participantCode,
          text: allValues.participantCode,
        },
      };
    });
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (loginForm.address.text !== '') {
        refreshAddress(loginForm.address.text);
      }
    }, 1000);
    return () => clearTimeout(timeout);
  }, [loginForm]);

  const history = useHistory();
  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    if (authError) {
      notification.error({
        message: 'ERROR',
        description: `${authError.code}: ${authError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
    if (authStatus) {
      history.push('/waiting');
    }
  }, [authError, authStatus, history, dispatch]);

  return (
    <Layout style={{ height: '100vh' }}>
      <Content>
        <Row align="middle" justify="center" style={{ height: '100%' }}>
          <Space align="center" direction="vertical" style={{ width: '100%' }}>
            <Title>Welcome To Testy</Title>
            <Card style={{ width: '35vh' }}>
              <Space
                align="center"
                direction="vertical"
                style={{ width: '100%' }}
              >
                <Title level={3}>Input Credentials</Title>
                <Form
                  onValuesChange={updateForm}
                  onFinish={loginRequest}
                  layout="vertical"
                  colon={false}
                >
                  <div style={{ textAlign: 'center' }}>Server Address</div>
                  <Form.Item name="address" rules={[{ required: true }]}>
                    <Input placeholder="IP Address/Domain" autoFocus />
                  </Form.Item>
                  <div style={{ textAlign: 'center' }}>Exam Password</div>
                  <Form.Item name="examPassword" rules={[{ required: true }]}>
                    <Input />
                  </Form.Item>
                  <div style={{ textAlign: 'center' }}>Participant Code</div>
                  <Form.Item
                    name="participantCode"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item>
                    <Space
                      align="center"
                      direction="vertical"
                      style={{ width: '100%' }}
                    >
                      <Button htmlType="submit" type="primary">
                        LOGIN
                      </Button>
                    </Space>
                  </Form.Item>
                </Form>
              </Space>
            </Card>
          </Space>
        </Row>
      </Content>
    </Layout>
  );
}
