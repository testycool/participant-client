import React, { useEffect } from 'react';
import { Layout, Row, Space, Spin, Typography } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from '../store';
import { requestGetExam } from '../ipcRenderer/senders/examSender';
import { ExamStatusModel } from '../../shared/models/ExamModel';

export default function Waiting(): JSX.Element {
  const { Content } = Layout;
  const { Title, Paragraph } = Typography;

  const exam = useSelector((state: RootState) => state.ExamReducer.exam);
  const participant = useSelector(
    (state: RootState) => state.ParticipantReducer.activeParticipant
  );

  const history = useHistory();
  useEffect(() => {
    if (exam?.status === ExamStatusModel.STARTED) {
      history.push('/attempt');
    }
    setTimeout(() => {
      if (exam) {
        requestGetExam(exam?.password);
      }
    }, 5000);
  }, [exam, history]);

  return (
    <Layout style={{ height: '100vh' }}>
      <Content>
        <Row align="middle" justify="center" style={{ height: '100%' }}>
          <Space align="center" direction="vertical" style={{ width: '100%' }}>
            <Spin indicator={<LoadingOutlined style={{ fontSize: '8em' }} />} />
            <Title>Waiting for the host to start the meeting...</Title>
            {exam?.startAt ? (
              <Paragraph style={{ textAlign: 'center' }}>
                The exam was scheduled to start at{' '}
                {new Date(exam.startAt).toLocaleString()}
              </Paragraph>
            ) : (
              ''
            )}
            {participant ? (
              <Paragraph style={{ textAlign: 'center' }}>
                You are logged in as {participant.name}
              </Paragraph>
            ) : (
              ''
            )}
          </Space>
        </Row>
      </Content>
    </Layout>
  );
}
