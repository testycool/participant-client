import React, { useEffect, useRef } from 'react';
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Layout,
  notification,
  Radio,
  Row,
  Space,
  Typography,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { SecondsToDuration } from '../../shared/libs/TimeParser';
import QuestionList from '../components/QuestionList';
import { AppDispatch, RootState } from '../store';
import { setQuestionError } from '../../shared/redux/actions/QuestionAction';
import {
  requestGetQuestion,
  requestListQuestions,
} from '../ipcRenderer/senders/questionSender';
import { QuestionTypeModel } from '../../shared/models/QuestionModel';
import QuestionControl from '../components/QuestionControl';

export default function Attempt(): JSX.Element {
  const { Paragraph, Title } = Typography;

  const exam = useSelector((state: RootState) => state.ExamReducer.exam);
  const questions = useSelector(
    (state: RootState) => state.QuestionReducer.questions
  );
  const activeQuestionIdx = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestionIdx
  );
  const activeQuestion = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestion
  );
  const choices = useSelector(
    (state: RootState) => state.ChoiceReducer.choices
  );
  // const questionLoading = useSelector(
  //   (state: RootState) => state.QuestionReducer.loading
  // );
  const questionError = useSelector(
    (state: RootState) => state.QuestionReducer.error
  );

  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    if (questionError) {
      notification.error({
        message: 'ERROR',
        description: `${questionError.code}: ${questionError.details}`,
      });
      dispatch(setQuestionError({ err: undefined }));
    }
  }, [questionError, dispatch]);

  useEffect(() => {
    if (exam?.id) {
      requestListQuestions(-1, 1, exam.id);
    }
  }, [exam]);

  useEffect(() => {
    if (questions.length > 0) {
      if (activeQuestion?.id !== questions[activeQuestionIdx].id) {
        requestGetQuestion(questions[activeQuestionIdx].id);
      }
    }
  }, [questions, activeQuestionIdx, activeQuestion, dispatch]);

  const remainingTime = (startAt: string, timeLimit: number) => {
    const now = new Date();
    const startDate = new Date(startAt);
    return timeLimit * 3600000 - Math.abs(now.getTime() - startDate.getTime());
  };

  const remainingTimeString = useRef('00:00:00');
  useEffect(() => {
    if (exam) {
      if (remainingTime(exam.startAt as string, exam.timeLimit) > 0) {
        setTimeout(() => {
          console.log('refresh time');
          remainingTimeString.current = SecondsToDuration(
            remainingTime(exam.startAt as string, exam.timeLimit)
          );
        }, 1000);
      }
    }
  }, [remainingTimeString, exam]);

  return (
    <Layout
      className="site-layout-background"
      style={{ minHeight: '100vh', width: '100%', padding: 48 }}
    >
      <Title style={{ textAlign: 'center' }}>
        {exam?.title || 'Exam Title'}
      </Title>
      <Row align="top" justify="start" gutter={10} wrap={false}>
        <Col flex="22em">
          <Card>
            <Space direction="vertical" style={{ width: '100%' }}>
              <Title level={3} style={{ textAlign: 'center' }}>
                Time Remaining
                <Title level={4} style={{ textAlign: 'center' }}>
                  {remainingTimeString.current}
                </Title>
              </Title>
              <QuestionList
                questions={questions}
                activeIndex={activeQuestionIdx}
              />
              <Button size="large" style={{ width: '100%' }}>
                FINISH ATTEMPT
              </Button>
            </Space>
          </Card>
        </Col>
        <Col flex="auto">
          <Card>
            <Col>
              <QuestionControl
                num={activeQuestionIdx + 1}
                saveAnswer={() => {}}
              />
              <Row>
                <Paragraph>{activeQuestion?.content}</Paragraph>
              </Row>
              <Row>
                <Col flex="100%">
                  <Form>
                    {activeQuestion?.type === QuestionTypeModel.MC ? (
                      <Form.Item name={['content', 'choiceId']}>
                        <Radio.Group>
                          {choices.map((choice) => (
                            <Row key={choice.id}>
                              <Radio value={choice.id}>
                                <Paragraph>{choice.content}</Paragraph>
                              </Radio>
                            </Row>
                          ))}
                        </Radio.Group>
                      </Form.Item>
                    ) : (
                      <Form.Item name={['content', 'essay', 'content']}>
                        <Input />
                      </Form.Item>
                    )}
                  </Form>
                </Col>
              </Row>
            </Col>
          </Card>
        </Col>
      </Row>
    </Layout>
  );
}
