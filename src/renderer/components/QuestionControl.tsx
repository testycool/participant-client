import React from 'react';
import { Button, Input, Row, Space, Spin, Typography } from 'antd';
import {
  FlagFilled,
  LeftOutlined,
  LoadingOutlined,
  RightOutlined,
  SaveFilled,
} from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import { changeQuestion } from '../../shared/redux/actions/QuestionAction';
import { LoadingType } from '../../shared/models/LoadingType';

export default function QuestionControl(props: {
  num: number;
  saveAnswer: () => void;
}): JSX.Element {
  const { Title } = Typography;
  const { num, saveAnswer } = props;

  const questions = useSelector(
    (state: RootState) => state.QuestionReducer.questions
  );
  const activeQuestionIdx = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestionIdx
  );
  const questionLoading = useSelector(
    (state: RootState) => state.QuestionReducer.loading
  );
  const choiceLoading = useSelector(
    (state: RootState) => state.ChoiceReducer.loading
  );

  const dispatch = useDispatch<AppDispatch>();
  const incrementIdx = (inc: number) => {
    const targetIndex = activeQuestionIdx + inc;
    dispatch(changeQuestion({ index: targetIndex }));
  };

  return (
    <Row align="middle" justify="space-between">
      <Space>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#0642CD',
            borderColor: '#0642CD',
          }}
          onClick={() => incrementIdx(-1)}
          disabled={
            activeQuestionIdx === 0 ||
            questionLoading !== LoadingType.Idle ||
            choiceLoading !== LoadingType.Idle
          }
        >
          <LeftOutlined />
        </Button>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#0642CD',
            borderColor: '#0642CD',
          }}
          onClick={() => incrementIdx(1)}
          disabled={
            activeQuestionIdx === questions.length - 1 ||
            questionLoading !== LoadingType.Idle ||
            choiceLoading !== LoadingType.Idle
          }
        >
          <RightOutlined />
        </Button>
        <Button
          type="primary"
          danger
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
          }}
        >
          <FlagFilled />
        </Button>
        <Input size="large" name="flag-note" placeholder="Flag note" />
      </Space>
      <Title level={3}>{`Number ${num}`}</Title>
      <Space>
        <Spin
          indicator={<LoadingOutlined />}
          tip="Saving your progress..."
          spinning={questionLoading !== LoadingType.Idle}
        >
          <Button
            style={{
              width: '3em',
              height: '3em',
              textAlign: 'center',
              padding: 0,
            }}
            disabled={
              questionLoading !== LoadingType.Idle ||
              choiceLoading !== LoadingType.Idle
            }
            onClick={saveAnswer}
          >
            <SaveFilled />
          </Button>
        </Spin>
      </Space>
    </Row>
  );
}
