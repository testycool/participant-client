import configureElectronStore from '../shared/redux/store/configureElectronStore';

export const rendererStore = configureElectronStore('renderer');

export type RootState = ReturnType<typeof rendererStore.getState>;
export type AppDispatch = typeof rendererStore.dispatch;
