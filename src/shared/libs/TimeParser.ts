// eslint-disable-next-line import/prefer-default-export
export const SecondsToDuration = (s?: number): string => {
  if (!s) {
    return '';
  }
  const hours = Math.floor(s / 3600);
  const minutes = Math.floor(s / 60) % 60;
  const seconds = s % 60;

  return [hours, minutes, seconds].map((v) => (v < 10 ? `0${v}` : v)).join(':');
};
