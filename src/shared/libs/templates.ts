import { CreateQuestionRequest } from '../../main/services/testycool/question';
import { TypeModelToNumber } from './QuestionTypeParser';
import { QuestionTypeModel, TextFormatModel } from '../models/QuestionModel';
import { FormatModelToNumber } from './TextFormatTypeParser';

// eslint-disable-next-line import/prefer-default-export
export const createEmptyQuestion = (examId: number): CreateQuestionRequest => ({
  examId,
  content: 'Question here...',
  type: TypeModelToNumber(QuestionTypeModel.MC),
  format: FormatModelToNumber(TextFormatModel.PLAIN),
});
