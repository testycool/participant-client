export enum QuestionTypeModel {
  UNRECOGNIZED,
  MC,
  ES,
}

export enum TextFormatModel {
  UNKNOWN,
  PLAIN,
  HTML,
  MARKDOWN,
}

export interface QuestionModel {
  id: number;
  type: QuestionTypeModel;
  format: TextFormatModel;
  content: string;
  examId: number;
}
