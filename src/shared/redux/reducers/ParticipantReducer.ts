import { createReducer } from '@reduxjs/toolkit';
import { getParticipant, listParticipants } from '../actions/ParticipantAction';
import { ParticipantModel } from '../../models/ParticipantModel';

export interface ExamState {
  size: number;
  page: number;
  totalSize?: number;
  participants: ParticipantModel[];
  activeParticipant?: ParticipantModel;
  loading: boolean;
  error?: any;
}

const initialState: ExamState = {
  size: -1,
  page: 1,
  totalSize: undefined,
  participants: [],
  activeParticipant: undefined,
  loading: false,
};

const ParticipantReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listParticipants.pending, (state) => ({
      ...state,
      loading: true,
    }))
    .addCase(listParticipants.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        participants: action.payload.participants,
        totalSize: action.payload.totalSize,
        loading: false,
      };
    })
    .addCase(listParticipants.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: false,
      };
    })
    .addCase(getParticipant.pending, (state) => ({
      ...state,
      loading: true,
    }))
    .addCase(getParticipant.fulfilled, (state, action) => {
      return {
        ...state,
        activeParticipant: action.payload.participant,
        loading: false,
      };
    })
    .addCase(getParticipant.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: false,
      };
    })
);

export default ParticipantReducer;
