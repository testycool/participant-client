import { createReducer } from '@reduxjs/toolkit';
import { ExamModel } from '../../models/ExamModel';
import { getExam } from '../actions/ExamAction';
import { setError } from '../actions/AuthAction';

export interface ExamState {
  exam?: ExamModel;
  loading: boolean;
  error?: any;
}

const initialState: ExamState = {
  exam: undefined,
  loading: false,
};

const ExamReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(getExam.pending, (state) => ({
      ...state,
      loading: true,
    }))
    .addCase(getExam.fulfilled, (state, action) => {
      return {
        ...state,
        exam: action.payload.exam,
        loading: false,
      };
    })
    .addCase(getExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: false,
      };
    })
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: false,
    }))
);

export default ExamReducer;
