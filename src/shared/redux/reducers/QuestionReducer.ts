import { createReducer } from '@reduxjs/toolkit';
import { setError } from '../actions/AuthAction';
import {
  changeQuestion,
  getQuestion,
  listQuestions,
} from '../actions/QuestionAction';
import { QuestionModel } from '../../models/QuestionModel';
import { LoadingType } from '../../models/LoadingType';

export interface QuestionState {
  size: number;
  page: number;
  totalSize?: number;
  questions: QuestionModel[];
  activeQuestionIdx: number;
  activeQuestion?: QuestionModel;
  loading: LoadingType;
  error?: any;
}

const initialState: QuestionState = {
  size: -1,
  page: 1,
  totalSize: undefined,
  questions: [],
  activeQuestionIdx: 0,
  activeQuestion: undefined,
  loading: LoadingType.Idle,
  error: undefined,
};

const QuestionReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listQuestions.pending, (state) => ({
      ...state,
      loading: LoadingType.GetList,
    }))
    .addCase(listQuestions.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        questions: action.payload.questions,
        totalSize: action.payload.totalSize,
        loading: LoadingType.Idle,
      };
    })
    .addCase(listQuestions.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.GetEntry,
    }))
    .addCase(getQuestion.fulfilled, (state, action) => {
      return {
        ...state,
        activeQuestion: action.payload.question,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(changeQuestion, (state, action) => {
      state.activeQuestionIdx = action.payload.index;
      state.loading = LoadingType.Idle;
    })
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: LoadingType.Idle,
    }))
);

export default QuestionReducer;
