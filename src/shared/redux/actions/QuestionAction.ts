import { createAction } from '@reduxjs/toolkit';
import { QuestionModel } from '../../models/QuestionModel';

export const listQuestions = {
  pending: createAction('[Question] List all questions/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    questions: QuestionModel[];
    totalSize: number;
  }>('[Question] List all questions/fulfilled'),
  rejected: createAction<{ err: any }>(
    '[Question] List all questions/rejected'
  ),
};

export const getQuestion = {
  pending: createAction('[Question] Get one question/pending'),
  fulfilled: createAction<{ question: QuestionModel }>(
    '[Question] Get one question/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Question] Get one question/rejected'),
};

export const changeQuestion = createAction<{ index: number }>(
  '[Question] change active question'
);

export const setQuestionError = createAction<{ err: any }>(
  '[Question] set question error'
);
