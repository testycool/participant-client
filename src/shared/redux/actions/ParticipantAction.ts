import { createAction } from '@reduxjs/toolkit';
import { ParticipantModel } from '../../models/ParticipantModel';

// eslint-disable-next-line import/prefer-default-export
export const listParticipants = {
  pending: createAction('[Participant] List all participants/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    participants: ParticipantModel[];
    totalSize: number;
  }>('[Participant] List all participants/fulfilled'),
  rejected: createAction<{ err: any }>(
    '[Participant] List all participants/rejected'
  ),
};

export const getParticipant = {
  pending: createAction('[Participant] Get participant/pending'),
  fulfilled: createAction<{ participant: ParticipantModel }>(
    '[Participant] Get participant/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Participant] Get participant/rejected'
  ),
};
