import { createAction } from '@reduxjs/toolkit';

export const setAddress = createAction<{ address: string }>(
  '[Auth] set backend server address'
);
export const setAccessToken = createAction<{ accessToken: string }>(
  '[Auth] set access token'
);

export const setAuthStatus = createAction<{ authenticated: boolean }>(
  '[Auth] set authentication status'
);

export const setError = createAction<{ err: any }>(
  '[Auth] set authentication error'
);
