import { createAction } from '@reduxjs/toolkit';
import ChoiceModel from '../../models/ChoiceModel';

export const listChoices = {
  pending: createAction('[Choice] List all choices/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    choices: ChoiceModel[];
    totalSize: number;
  }>('[Choice] List all choices/fulfilled'),
  rejected: createAction<{ err: any }>('[Choice] List all choices/rejected'),
};
export const clearChoices = createAction('[Choice] Clear choices variable');

export const setError = createAction<{ err: any }>('[Choice] set choice error');
