import { createAction } from '@reduxjs/toolkit';
import { ExamModel } from '../../models/ExamModel';

export const getExam = {
  pending: createAction('[Exam] Get one exam/pending'),
  fulfilled: createAction<{ exam: ExamModel }>('[Exam] Get one exam/fulfilled'),
  rejected: createAction<{ err: any }>('[Exam] Get one exam/rejected'),
};

export const setError = createAction<{ err: any }>(
  '[Auth] set authentication error'
);

export const removeActiveExam = createAction('[Exam] remove active exam');
