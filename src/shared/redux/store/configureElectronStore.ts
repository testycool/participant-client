import { configureStore } from '@reduxjs/toolkit';
import {
  AuthReducer,
  ChoiceReducer,
  ExamReducer,
  ParticipantReducer,
  QuestionReducer,
} from '../reducers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const configureElectronStore = (_scope = 'main') => {
  return configureStore({
    reducer: {
      AuthReducer,
      ExamReducer,
      ParticipantReducer,
      QuestionReducer,
      ChoiceReducer,
    },
  });
};

export default configureElectronStore;
