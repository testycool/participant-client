const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('auth', {
  ipcRenderer: {
    refreshAddress(address) {
      ipcRenderer.send('refresh-address', { address });
    },
    getToken(examPassword, participantCode) {
      ipcRenderer.send('get-token', { examPassword, participantCode });
    },
    once(channel, func) {
      const validChannels = ['refresh-address', 'get-token', 'auth-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = ['refresh-address', 'get-token', 'auth-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('exam', {
  ipcRenderer: {
    getExam(examPassword) {
      ipcRenderer.send('get-exam', { examPassword });
    },
    once(channel, func) {
      const validChannels = ['get-exam', 'exam-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = ['get-exam', 'exam-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('participant', {
  ipcRenderer: {
    listParticipants(size, page, examId) {
      ipcRenderer.send('list-participants', { size, page, examId });
    },
    getParticipant(id) {
      ipcRenderer.send('get-participant', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-participants',
        'get-participant',
        'participant-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-participants',
        'get-participant',
        'participant-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('question', {
  ipcRenderer: {
    listQuestions(size, page, examId) {
      ipcRenderer.send('list-questions', { size, page, examId });
    },
    getQuestion(id) {
      ipcRenderer.send('get-question', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-questions',
        'get-question',
        'question-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-questions',
        'get-question',
        'question-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('choice', {
  ipcRenderer: {
    listChoices(size, page, questionId) {
      ipcRenderer.send('list-choices', { size, page, filter: { questionId } });
    },
    once(channel, func) {
      const validChannels = ['list-choices', 'choice-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = ['list-choices', 'choice-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});
