import { ipcMain } from 'electron';
import { Metadata } from '@grpc/grpc-js';
import Services from '../services/services';
import mainStore from '../store';
import { GetParticipantRequest } from '../services/testycool/participant';

export default function participant() {
  ipcMain.on('list-participants', (event, args) => {
    Services.participant?.listParticipants(
      { size: args.size, page: args.page, filter: { examId: args.examId } },
      (err, response) => {
        if (err) {
          event.sender.send('participant-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('list-participants', response);
        }
      }
    );
  });
  ipcMain.on('get-participant', (event, args: GetParticipantRequest) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.participant?.getParticipant(
      { id: args.id },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('participant-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('get-participant', response);
        }
      }
    );
  });
}
