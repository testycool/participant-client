import { ipcMain } from 'electron';
import {
  CreateChoiceRequest,
  DeleteChoiceRequest,
  ListChoicesRequest,
  ListChoicesResponse,
} from '../services/testycool/choice';
import { TextFormat } from '../services/testycool/text_format';
import ChoiceModel from '../../shared/models/ChoiceModel';
import { NumberToFormatModel } from '../../shared/libs/TextFormatTypeParser';

export default function choice() {
  ipcMain.on('list-choices', (event, args: ListChoicesRequest) => {
    const dummyChoices: ListChoicesResponse = {
      size: 3,
      page: 1,
      totalSize: 3,
      choices: [
        {
          id: (args.filter?.questionId || 1) + 1,
          questionId: args.filter?.questionId || 1,
          format: TextFormat.TEXT_FORMAT_PLAIN,
          isCorrect: true,
          content: `Question ${(args.filter?.questionId || 1) + 1} 1`,
        },
        {
          id: (args.filter?.questionId || 1) + 2,
          questionId: args.filter?.questionId || 1,
          format: TextFormat.TEXT_FORMAT_PLAIN,
          isCorrect: false,
          content: `Question ${(args.filter?.questionId || 1) + 1} 2`,
        },
        {
          id: (args.filter?.questionId || 1) + 3,
          questionId: args.filter?.questionId || 1,
          format: TextFormat.TEXT_FORMAT_PLAIN,
          isCorrect: false,
          content: `Question ${(args.filter?.questionId || 1) + 1} 3`,
        },
      ],
    };
    console.log(args);
    event.reply('list-choices', dummyChoices);
  });
  ipcMain.on('create-choice', (event, args: CreateChoiceRequest) => {
    console.log(args);
    event.reply('create-choice', {
      ...args,
      id: 1000,
      type: 1,
    });
  });
  ipcMain.on(
    'update-choice',
    (
      event,
      args: {
        updatedChoice: ChoiceModel;
      }
    ) => {
      console.log(args);
      event.reply('update-choice', {
        ...args.updatedChoice,
        format: NumberToFormatModel(args.updatedChoice.format),
      });
    }
  );
  ipcMain.on('delete-choice', (event, args: DeleteChoiceRequest) => {
    console.log(`delete ${args.id}`);
    event.reply('delete-choice', args.id);
  });
}
