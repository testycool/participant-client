import { ipcMain } from 'electron';
import { Metadata } from '@grpc/grpc-js';
import mainStore from '../store';
import Services from '../services/services';

const exam = () => {
  ipcMain.on('get-exam', (event, args: { examPassword: string }) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.getExam(
      { id: undefined, password: args.examPassword },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('exam-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('get-exam', response);
        }
      }
    );
  });
};

export default exam;
