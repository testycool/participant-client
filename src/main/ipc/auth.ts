import { ipcMain } from 'electron';
import { ChannelCredentials } from '@grpc/grpc-js';
import mainStore from '../store';
import Services from '../services/services';
import {
  AuthServiceClient,
  GetParticipantTokenRequest,
} from '../services/testycool/auth';
import {
  setAccessToken,
  setAddress,
} from '../../shared/redux/actions/AuthAction';
import { ParticipantServiceClient } from '../services/testycool/participant';
import { ExamServiceClient } from '../services/testycool/exam';

export default function auth() {
  ipcMain.on('refresh-address', (_event, args) => {
    const { address } = mainStore.getState().AuthReducer;
    if (address === undefined || address !== args.address) {
      Services.auth = new AuthServiceClient(
        args.address,
        ChannelCredentials.createInsecure()
      );
      Services.exam = new ExamServiceClient(
        args.address,
        ChannelCredentials.createInsecure()
      );
      Services.participant = new ParticipantServiceClient(
        args.address,
        ChannelCredentials.createInsecure()
      );
      mainStore.dispatch(setAddress({ address: args.address }));
    }
  });
  ipcMain.on('get-token', (event, args: GetParticipantTokenRequest) => {
    Services.auth?.getParticipantToken(
      {
        examPassword: args.examPassword,
        participantCode: args.participantCode,
      },
      (err, response) => {
        if (err) {
          event.sender.send('auth-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          const token = response.accessToken;
          const payload = token.split('.')[1];
          const participantId = JSON.parse(global.atob(payload)).user_id;
          event.reply('get-token', {
            accessToken: response.accessToken,
            examPassword: args.examPassword,
            participantId,
          });
          mainStore.dispatch(
            setAccessToken({ accessToken: response.accessToken })
          );
        }
      }
    );
  });
}
