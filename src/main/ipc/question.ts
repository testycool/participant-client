import { ipcMain } from 'electron';
import { ChannelCredentials, Metadata } from '@grpc/grpc-js';
import mainStore from '../store';
import Services from '../services/services';
import { QuestionServiceClient } from '../services/testycool/question';

export default function question() {
  ipcMain.on(
    'list-questions',
    (event, args: { size: number; page: number; examId: number }) => {
      const { address } = mainStore.getState().AuthReducer;
      if (!Services.question && address) {
        Services.question = new QuestionServiceClient(
          address,
          ChannelCredentials.createInsecure()
        );
      }
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.question?.listQuestions(
        {
          size: args.size,
          page: args.page,
          filter: { examId: args.examId },
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('question-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('list-questions', response);
          }
        }
      );
    }
  );
  ipcMain.on('get-question', (event, args: { id: number }) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.question?.getQuestion(
      {
        id: args.id,
      },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('question-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('get-question', response);
        }
      }
    );
  });
}
