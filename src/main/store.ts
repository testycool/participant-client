import configureElectronStore from '../shared/redux/store/configureElectronStore';

const mainStore = configureElectronStore('main');

export default mainStore;
