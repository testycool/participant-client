/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetParticipantRequest {
  id: number;
}

export interface GetParticipantResponse {
  participant: Participant | undefined;
}

export interface ListParticipantsRequest {
  size: number;
  page: number;
  filter: ListParticipantsRequest_Filter | undefined;
}

export interface ListParticipantsRequest_Filter {
  examId?: number | undefined;
}

export interface ListParticipantsResponse {
  participants: Participant[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateParticipantRequest {
  examId: number;
  code: string;
  name: string;
}

export interface CreateParticipantResponse {
  participant: Participant | undefined;
}

export interface UpdateParticipantRequest {
  participant: Participant | undefined;
}

export interface UpdateParticipantResponse {
  participant: Participant | undefined;
}

export interface DeleteParticipantRequest {
  id: number;
}

export interface Participant {
  id: number;
  examId: number;
  code: string;
  name: string;
}

const baseGetParticipantRequest: object = { id: 0 };

export const GetParticipantRequest = {
  encode(
    message: GetParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetParticipantRequest } as GetParticipantRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantRequest {
    const message = { ...baseGetParticipantRequest } as GetParticipantRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: GetParticipantRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<GetParticipantRequest>
  ): GetParticipantRequest {
    const message = { ...baseGetParticipantRequest } as GetParticipantRequest;
    message.id = object.id ?? 0;
    return message;
  },
};

const baseGetParticipantResponse: object = {};

export const GetParticipantResponse = {
  encode(
    message: GetParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetParticipantResponse } as GetParticipantResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantResponse {
    const message = { ...baseGetParticipantResponse } as GetParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromJSON(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },

  toJSON(message: GetParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<GetParticipantResponse>
  ): GetParticipantResponse {
    const message = { ...baseGetParticipantResponse } as GetParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromPartial(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },
};

const baseListParticipantsRequest: object = { size: 0, page: 0 };

export const ListParticipantsRequest = {
  encode(
    message: ListParticipantsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListParticipantsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListParticipantsRequest,
    } as ListParticipantsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListParticipantsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsRequest {
    const message = {
      ...baseListParticipantsRequest,
    } as ListParticipantsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListParticipantsRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListParticipantsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListParticipantsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListParticipantsRequest>
  ): ListParticipantsRequest {
    const message = {
      ...baseListParticipantsRequest,
    } as ListParticipantsRequest;
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListParticipantsRequest_Filter.fromPartial(
        object.filter
      );
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListParticipantsRequest_Filter: object = {};

export const ListParticipantsRequest_Filter = {
  encode(
    message: ListParticipantsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(8).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListParticipantsRequest_Filter,
    } as ListParticipantsRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsRequest_Filter {
    const message = {
      ...baseListParticipantsRequest_Filter,
    } as ListParticipantsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = undefined;
    }
    return message;
  },

  toJSON(message: ListParticipantsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListParticipantsRequest_Filter>
  ): ListParticipantsRequest_Filter {
    const message = {
      ...baseListParticipantsRequest_Filter,
    } as ListParticipantsRequest_Filter;
    message.examId = object.examId ?? undefined;
    return message;
  },
};

const baseListParticipantsResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListParticipantsResponse = {
  encode(
    message: ListParticipantsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.participants) {
      Participant.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListParticipantsResponse,
    } as ListParticipantsResponse;
    message.participants = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participants.push(
            Participant.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsResponse {
    const message = {
      ...baseListParticipantsResponse,
    } as ListParticipantsResponse;
    message.participants = [];
    if (object.participants !== undefined && object.participants !== null) {
      for (const e of object.participants) {
        message.participants.push(Participant.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListParticipantsResponse): unknown {
    const obj: any = {};
    if (message.participants) {
      obj.participants = message.participants.map((e) =>
        e ? Participant.toJSON(e) : undefined
      );
    } else {
      obj.participants = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListParticipantsResponse>
  ): ListParticipantsResponse {
    const message = {
      ...baseListParticipantsResponse,
    } as ListParticipantsResponse;
    message.participants = [];
    if (object.participants !== undefined && object.participants !== null) {
      for (const e of object.participants) {
        message.participants.push(Participant.fromPartial(e));
      }
    }
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

const baseCreateParticipantRequest: object = { examId: 0, code: '', name: '' };

export const CreateParticipantRequest = {
  encode(
    message: CreateParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== 0) {
      writer.uint32(8).int32(message.examId);
    }
    if (message.code !== '') {
      writer.uint32(18).string(message.code);
    }
    if (message.name !== '') {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCreateParticipantRequest,
    } as CreateParticipantRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        case 2:
          message.code = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateParticipantRequest {
    const message = {
      ...baseCreateParticipantRequest,
    } as CreateParticipantRequest;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.code !== undefined && object.code !== null) {
      message.code = String(object.code);
    } else {
      message.code = '';
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = '';
    }
    return message;
  },

  toJSON(message: CreateParticipantRequest): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    message.code !== undefined && (obj.code = message.code);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateParticipantRequest>
  ): CreateParticipantRequest {
    const message = {
      ...baseCreateParticipantRequest,
    } as CreateParticipantRequest;
    message.examId = object.examId ?? 0;
    message.code = object.code ?? '';
    message.name = object.name ?? '';
    return message;
  },
};

const baseCreateParticipantResponse: object = {};

export const CreateParticipantResponse = {
  encode(
    message: CreateParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCreateParticipantResponse,
    } as CreateParticipantResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateParticipantResponse {
    const message = {
      ...baseCreateParticipantResponse,
    } as CreateParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromJSON(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },

  toJSON(message: CreateParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateParticipantResponse>
  ): CreateParticipantResponse {
    const message = {
      ...baseCreateParticipantResponse,
    } as CreateParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromPartial(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },
};

const baseUpdateParticipantRequest: object = {};

export const UpdateParticipantRequest = {
  encode(
    message: UpdateParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseUpdateParticipantRequest,
    } as UpdateParticipantRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateParticipantRequest {
    const message = {
      ...baseUpdateParticipantRequest,
    } as UpdateParticipantRequest;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromJSON(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },

  toJSON(message: UpdateParticipantRequest): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateParticipantRequest>
  ): UpdateParticipantRequest {
    const message = {
      ...baseUpdateParticipantRequest,
    } as UpdateParticipantRequest;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromPartial(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },
};

const baseUpdateParticipantResponse: object = {};

export const UpdateParticipantResponse = {
  encode(
    message: UpdateParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseUpdateParticipantResponse,
    } as UpdateParticipantResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateParticipantResponse {
    const message = {
      ...baseUpdateParticipantResponse,
    } as UpdateParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromJSON(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },

  toJSON(message: UpdateParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateParticipantResponse>
  ): UpdateParticipantResponse {
    const message = {
      ...baseUpdateParticipantResponse,
    } as UpdateParticipantResponse;
    if (object.participant !== undefined && object.participant !== null) {
      message.participant = Participant.fromPartial(object.participant);
    } else {
      message.participant = undefined;
    }
    return message;
  },
};

const baseDeleteParticipantRequest: object = { id: 0 };

export const DeleteParticipantRequest = {
  encode(
    message: DeleteParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseDeleteParticipantRequest,
    } as DeleteParticipantRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteParticipantRequest {
    const message = {
      ...baseDeleteParticipantRequest,
    } as DeleteParticipantRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteParticipantRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<DeleteParticipantRequest>
  ): DeleteParticipantRequest {
    const message = {
      ...baseDeleteParticipantRequest,
    } as DeleteParticipantRequest;
    message.id = object.id ?? 0;
    return message;
  },
};

const baseParticipant: object = { id: 0, examId: 0, code: '', name: '' };

export const Participant = {
  encode(
    message: Participant,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.code !== '') {
      writer.uint32(26).string(message.code);
    }
    if (message.name !== '') {
      writer.uint32(34).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Participant {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseParticipant } as Participant;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.code = reader.string();
          break;
        case 4:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Participant {
    const message = { ...baseParticipant } as Participant;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.code !== undefined && object.code !== null) {
      message.code = String(object.code);
    } else {
      message.code = '';
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = '';
    }
    return message;
  },

  toJSON(message: Participant): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.examId !== undefined && (obj.examId = message.examId);
    message.code !== undefined && (obj.code = message.code);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<Participant>): Participant {
    const message = { ...baseParticipant } as Participant;
    message.id = object.id ?? 0;
    message.examId = object.examId ?? 0;
    message.code = object.code ?? '';
    message.name = object.name ?? '';
    return message;
  },
};

export const ParticipantServiceService = {
  getParticipant: {
    path: '/testycool.v1.ParticipantService/GetParticipant',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetParticipantRequest) =>
      Buffer.from(GetParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetParticipantRequest.decode(value),
    responseSerialize: (value: GetParticipantResponse) =>
      Buffer.from(GetParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      GetParticipantResponse.decode(value),
  },
  listParticipants: {
    path: '/testycool.v1.ParticipantService/ListParticipants',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListParticipantsRequest) =>
      Buffer.from(ListParticipantsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ListParticipantsRequest.decode(value),
    responseSerialize: (value: ListParticipantsResponse) =>
      Buffer.from(ListParticipantsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ListParticipantsResponse.decode(value),
  },
  createParticipant: {
    path: '/testycool.v1.ParticipantService/CreateParticipant',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateParticipantRequest) =>
      Buffer.from(CreateParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CreateParticipantRequest.decode(value),
    responseSerialize: (value: CreateParticipantResponse) =>
      Buffer.from(CreateParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateParticipantResponse.decode(value),
  },
  updateParticipant: {
    path: '/testycool.v1.ParticipantService/UpdateParticipant',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateParticipantRequest) =>
      Buffer.from(UpdateParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      UpdateParticipantRequest.decode(value),
    responseSerialize: (value: UpdateParticipantResponse) =>
      Buffer.from(UpdateParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateParticipantResponse.decode(value),
  },
  deleteParticipant: {
    path: '/testycool.v1.ParticipantService/DeleteParticipant',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteParticipantRequest) =>
      Buffer.from(DeleteParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      DeleteParticipantRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface ParticipantServiceServer extends UntypedServiceImplementation {
  getParticipant: handleUnaryCall<
    GetParticipantRequest,
    GetParticipantResponse
  >;
  listParticipants: handleUnaryCall<
    ListParticipantsRequest,
    ListParticipantsResponse
  >;
  createParticipant: handleUnaryCall<
    CreateParticipantRequest,
    CreateParticipantResponse
  >;
  updateParticipant: handleUnaryCall<
    UpdateParticipantRequest,
    UpdateParticipantResponse
  >;
  deleteParticipant: handleUnaryCall<DeleteParticipantRequest, Empty>;
}

export interface ParticipantServiceClient extends Client {
  getParticipant(
    request: GetParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  getParticipant(
    request: GetParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  getParticipant(
    request: GetParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const ParticipantServiceClient = makeGenericClientConstructor(
  ParticipantServiceService,
  'testycool.v1.ParticipantService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ParticipantServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
