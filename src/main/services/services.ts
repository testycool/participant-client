import { ChannelCredentials } from '@grpc/grpc-js';
import { AuthServiceClient } from './testycool/auth';
import mainStore from '../store';
import { ExamServiceClient } from './testycool/exam';
import { ParticipantServiceClient } from './testycool/participant';
import { QuestionServiceClient } from './testycool/question';

const services: {
  auth: AuthServiceClient | undefined;
  exam: ExamServiceClient | undefined;
  participant: ParticipantServiceClient | undefined;
  question: QuestionServiceClient | undefined;
} = {
  auth: undefined,
  exam: undefined,
  participant: undefined,
  question: undefined,
};

const Services = {
  auth: ((address: string | undefined) => {
    if (!services.auth && address) {
      return new AuthServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    return services.auth;
  })(mainStore ? mainStore.getState().AuthReducer.address : undefined),
  exam: ((address: string | undefined) => {
    if (!services.exam && address) {
      return new ExamServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    return services.exam;
  })(mainStore ? mainStore.getState().AuthReducer.address : undefined),
  participant: ((address: string | undefined) => {
    if (!services.participant && address) {
      return new ParticipantServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    return services.participant;
  })(mainStore ? mainStore.getState().AuthReducer.address : undefined),
  question: ((address: string | undefined) => {
    if (!services.question && address) {
      return new QuestionServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    return services.question;
  })(mainStore ? mainStore.getState().AuthReducer.address : undefined),
};

export default Services;
